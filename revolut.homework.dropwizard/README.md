# RevolutInterviewAssignment

Why there's no DI framework?
---

I made WELD implementation of JavaEE CDI work but couldn't make it work with hibernate because dropwizard creates the
data source / session factory during boot and there's no way to register a bean programmatically with CDI, it has
to be created by it. A future improvement could be a research into integrating with some other DI framework.

How to start the RevolutInterviewAssignment application
---

1. Run `mvn clean install` or `mvn clean package` to build your application
1. Start application with `java -jar target/revolut.homework.dropwizard-1.jar server server.yml`
1. To check that your application is running enter url `http://localhost:8080`

Postman sample HTTP requests
---

https://www.getpostman.com/collections/511f58946bdafbb54be6

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
