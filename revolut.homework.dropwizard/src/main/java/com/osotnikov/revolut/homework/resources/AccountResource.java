package com.osotnikov.revolut.homework.resources;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osotnikov.revolut.homework.dao.entity.Account;
import com.osotnikov.revolut.homework.rest.response.JsonResponseBody;
import com.osotnikov.revolut.homework.service.DatabaseService;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/account")
public class AccountResource {
	
	private final static Logger logger = LoggerFactory.getLogger(AccountResource.class);

	DatabaseService databaseService;

	public AccountResource(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAccount(@PathParam("id") long id) {
		
		logger.debug("GET: Called with parameter: {}", id);
		
		Account account = databaseService.fetchById(Account.class, id);
		
		if(account == null) {
			String responseMsg = String.format("Account not found for id: %d", id);
			logger.debug(responseMsg);
			return Response.ok(new JsonResponseBody(404, responseMsg), MediaType.APPLICATION_JSON).status(404).build();
		}
		
		logger.debug("GET: Will return: {}", account);
		return Response.status(200).entity(account).build();
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response postAccount(@Valid Account account) {

		logger.debug("POST: Called with parameter: {}", ReflectionToStringBuilder.toString(account)); // I didn't include this in Account.toString in order not to tie the POJO to any dependencies
		
		String responseMsg = "";
		
		try {
			
			databaseService.createInStore(Account.class, account);
			
			responseMsg = "Account is stored : " + ReflectionToStringBuilder.toString(account);
			logger.info(responseMsg);
			
		} catch (EntityExistsException e) {
			
			logger.info(e.getMessage(), e);
			return Response.ok(new JsonResponseBody(400, e.getMessage()), MediaType.APPLICATION_JSON).status(400).build();
		} catch (Exception e) {
			
			logger.error(e.getMessage(), e);
			return Response.status(500).entity(new JsonResponseBody(500, e.getMessage())).build();
		}
		
		return Response.status(201).entity(new JsonResponseBody(201, "Account created successfully!")).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response putAccount(@Valid Account account) {

		logger.debug("PUT: Called with parameter: {}", ReflectionToStringBuilder.toString(account)); // I didn't include this in Account.toString in order not to tie the POJO to any dependencies
		
		String responseMsg = "";
		
		try {
			
			databaseService.updateExisting(Account.class, account);
			
			responseMsg = "Account is updated : " + ReflectionToStringBuilder.toString(account);
			logger.info(responseMsg);
			
		} catch (EntityNotFoundException e) {
			
			logger.info(e.getMessage(), e);
			return Response.status(400).entity(new JsonResponseBody(400, e.getMessage())).build();
		} catch (Exception e) {
			
			logger.error(e.getMessage(), e);
			return Response.status(500).entity(new JsonResponseBody(500, e.getMessage())).build();
		}
		
		return Response.status(200).entity(new JsonResponseBody(200, responseMsg)).build();
	}

}
