package com.osotnikov.revolut.homework.dropwizard.config;

import com.osotnikov.revolut.homework.RevolutInterviewAssignmentConfiguration;
import com.osotnikov.revolut.homework.dao.entity.Account;
import com.osotnikov.revolut.homework.dao.entity.Transfer;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;

public class RevolutHibernateBundle extends HibernateBundle<RevolutInterviewAssignmentConfiguration> {

	public RevolutHibernateBundle() {
		super(
			Account.class, 
			Transfer.class);
	}
	
	@Override
    public DataSourceFactory getDataSourceFactory(RevolutInterviewAssignmentConfiguration configuration) {
        return configuration.getDataSourceFactory();
    }
	
}
