package com.osotnikov.revolut.homework.service;

import static java.util.Objects.requireNonNull;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.osotnikov.revolut.homework.dao.entity.IdentifiableEntity;

import io.dropwizard.hibernate.UnitOfWork;

public class DatabaseService {
	
	private SessionFactory sessionFactory;

	/**
     * Returns the current {@link Session}.
     *
     * @return the current session
     */
    public Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
	
	public DatabaseService(SessionFactory sessionFactory) {
        this.sessionFactory = requireNonNull(sessionFactory);
    }
	
	/**
	 * Returns the entity for the provided id or null if no entity exists for id.
	 * 
	 * */
	@UnitOfWork
	public <E extends IdentifiableEntity> E fetchById(Class<E> entityClass, long id) {

		E entity = currentSession().find(entityClass, id);

		return entity;
	}
	
	/**
	 * Returns the entity for the provided id or null if no entity exists for id.
	 * 
	 * */
	@UnitOfWork
	public <E extends IdentifiableEntity> E fetchByIdWithException(Class<E> entityClass, long id) {
		
		E entity = currentSession().find(entityClass, id);
		if(entity == null) {
			throw new EntityNotFoundException(String.format("Could not find entity %s with id %d", entityClass.getCanonicalName(), id));
		}
		
		return entity;
	}
	
	/**
	 * Returns true iff entity.getId() == id. False otherwise.
	 * 
	 * */
	@UnitOfWork
	public <E extends IdentifiableEntity> boolean entityExists(Class<E> entityClass, long id) {
		
		E entity = fetchById(entityClass, id);
		
		if(entity != null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Will throw runtime exception (javax.persistence.EntityExistsException) if an entity with an 
	 * id equal to entity.getId() already exists.
	 * 
	 * */
	@UnitOfWork
	public <E extends IdentifiableEntity> void createInStore(Class<E> entityClass, E entity) {		
		
		if(!entityExists(entityClass, entity.getId())) { // Persist if does not exist.
			
			currentSession().persist(entity);
		} else {
			
			// Make an explicit throw because EntityManager.persist does not guarantee that it will be thrown,
			// it says that it <b>may</b> be thrown.
			
			String errorMsg = String.format("An entity with the id %d already exists, entity is: %s", 
					entity.getId(), ReflectionToStringBuilder.toString(entity));
			
			throw new EntityExistsException(errorMsg); 
		}	
	}
	
	/**
	 * Update already stored entity with entity.getId() with all the new values in entity. 
	 * Iff an entity with entity.getId() does not already exist inside the database throw
	 * javax.persistence.EntityNotFoundException runtime exception.
	 * 
	 * */
	@UnitOfWork
	public <E extends IdentifiableEntity> void updateExisting(Class<E> entityClass, E entity) {
		
		// Preconditions
		
		if(entity == null || entity.getId() == null) {
			
			throw new IllegalArgumentException(
				String.format("Null values are not allowed for entity: %s, entity.getId(): %d", entity, entity.getId()));
		}
		
		// Persist if does not exist.
		
		if(entityExists(entityClass, entity.getId())) { 
			
			currentSession().merge(entity);
		} else {
			
			// Make an explicit throw because EntityManager.persist does not guarantee that it will be thrown,
			// it says that it <b>may</b> be thrown.
			
			String errorMsg = String.format("An entity with the id %d does not exist, new entity values were supposed to be: %s", 
					entity.getId(), ReflectionToStringBuilder.toString(entity));
			
			throw new EntityNotFoundException(errorMsg); 
		}
	}
	
}
