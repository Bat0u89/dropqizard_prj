package com.osotnikov.revolut.homework.dao.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Transfer implements IdentifiableEntity {

	@Id
	@NotNull
	private Long id;
	@NotNull
	private Long benefactorId;
	@NotNull
	private Long beneficiaryId;
	@Digits(fraction=2, integer = 100)
	@Min(0)
	@NotNull
	private BigDecimal transferedAmount;
	
	@Override
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Long getBenefactorId() {
		return benefactorId;
	}
	public void setBenefactorId(Long benefactorId) {
		this.benefactorId = benefactorId;
	}
	public Long getBeneficiaryId() {
		return beneficiaryId;
	}
	public void setBeneficiaryId(Long beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}
	public BigDecimal getTransferedAmount() {
		return transferedAmount;
	}
	public void setTransferedAmount(BigDecimal transferedAmount) {
		this.transferedAmount = transferedAmount;
	}

}
