package com.osotnikov.revolut.homework.rest.exception.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.osotnikov.revolut.homework.rest.response.JsonResponseBody;

/**
 * This class is needed in order to display error messages as JSON response bodies
 * of the requests sent to REST endpoints of this application.
 * 
 * */
@Singleton
@Provider
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException> {

@Override
public Response toResponse(ConstraintViolationException e) {
    // There can be multiple constraint Violations
    Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
    List<String> messages = new ArrayList<>();
    for (ConstraintViolation<?> violation : violations) {
    	
        messages.add(violation.getPropertyPath() + " : " + violation.getMessage());

    }
    return Response.status(400).entity(new JsonResponseBody(400, StringUtils.join(messages,", "))).build(); // Response.status(Status.BAD_REQUEST).entity(StringUtils.join(messages,";")).build();
}

}