package com.osotnikov.revolut.homework.resources;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osotnikov.revolut.homework.dao.entity.Transfer;
import com.osotnikov.revolut.homework.rest.response.JsonResponseBody;
import com.osotnikov.revolut.homework.service.DatabaseService;
import com.osotnikov.revolut.homework.service.TransferService;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/transfer")
public class TransferResource {

	private final static Logger logger = LoggerFactory.getLogger(TransferResource.class);
	
	TransferService transferService;

	DatabaseService databaseService;

	public TransferResource(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getTransfer(@PathParam("id") long id) {
		
		logger.debug("GET: Called with parameter: {}", id);
		
		Transfer transfer = databaseService.fetchById(Transfer.class, id);
		
		if(transfer == null) {
			String responseMsg = String.format("Transfer not found for id: %d", id);
			logger.debug(responseMsg);
			return Response.status(404).entity(new JsonResponseBody(404, responseMsg)).build();
		}
		
		logger.debug("GET: Will return: {}", transfer);
		return Response.status(200).entity(transfer).build();
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response postTransfer(@Valid Transfer transfer) {

		logger.debug("POST: Called with parameter: {}", ReflectionToStringBuilder.toString(transfer)); // I didn't include this in Transfer.toString in order not to tie the POJO to any dependencies
		
		String responseMsg = "";
		
		try {
			
			transferService.transfer(transfer);
			
			responseMsg = "Transfer is completed : " + ReflectionToStringBuilder.toString(transfer);
			logger.info(responseMsg);
			
		} catch (EntityNotFoundException e) {
			
			logger.info(e.getMessage(), e);
			return Response.status(400).entity(new JsonResponseBody(400, e.getMessage())).build();
		} catch (Exception e) {
			
			logger.error(e.getMessage(), e);
			return Response.status(500).entity(new JsonResponseBody(500, e.getMessage())).build();
		}
		
		return Response.status(201).entity(new JsonResponseBody(201, "Transfer created successfully!")).build();
	}
	
}
