package com.osotnikov.revolut.homework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osotnikov.revolut.homework.dropwizard.config.RevolutHibernateBundle;
import com.osotnikov.revolut.homework.resources.AccountResource;
import com.osotnikov.revolut.homework.resources.TransferResource;
import com.osotnikov.revolut.homework.service.DatabaseService;

import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class RevolutInterviewAssignmentApplication extends Application<RevolutInterviewAssignmentConfiguration> {

	private final static Logger logger = LoggerFactory.getLogger(AccountResource.class);
	
	public static HibernateBundle<RevolutInterviewAssignmentConfiguration> hibernateBundle = new RevolutHibernateBundle();
	
    public static void main(final String[] args) throws Exception {

    	new RevolutInterviewAssignmentApplication().run(args);
    }

    @Override
    public String getName() {
        return "RevolutInterviewAssignment";
    }

    @Override
    public void initialize(final Bootstrap<RevolutInterviewAssignmentConfiguration> bootstrap) {

    	logger.info("pre weld bundle!");
    	
    	// Read config file from inside the jar
    	bootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
    	
		// JPA EntityManagerSupport
		bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(final RevolutInterviewAssignmentConfiguration configuration,
                    final Environment environment) {
        
    	logger.info("the spot!");

    	final DatabaseService databaseService = new DatabaseService(hibernateBundle.getSessionFactory());

    	environment.jersey().register(new AccountResource(databaseService));
    	environment.jersey().register(new TransferResource(databaseService));
    }

}
