package com.osotnikov.revolut.homework.service;

import com.osotnikov.revolut.homework.dao.entity.Account;
import com.osotnikov.revolut.homework.dao.entity.Transfer;

import io.dropwizard.hibernate.UnitOfWork;

public class TransferService {
	
	DatabaseService databaseService;

	public TransferService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	@UnitOfWork
	public void transfer(Transfer transfer) {

		Account benefactor = databaseService.fetchByIdWithException(Account.class, transfer.getBenefactorId());
		Account beneficiary = databaseService.fetchByIdWithException(Account.class, transfer.getBeneficiaryId());
		
		if(!benefactor.getCurrencyCode().equals(beneficiary.getCurrencyCode())) {
			
			throw new IllegalArgumentException(String.format("The currencies of the accounts with IDs %d, %d do not match!", 
				transfer.getBenefactorId(), transfer.getBeneficiaryId()));
		}
		
		benefactor.setBalance(benefactor.getBalance().subtract(transfer.getTransferedAmount()));
		beneficiary.setBalance(beneficiary.getBalance().add(transfer.getTransferedAmount()));
		
		databaseService.updateExisting(Account.class, benefactor);
		databaseService.updateExisting(Account.class, beneficiary);
		
		databaseService.createInStore(Transfer.class, transfer);
	}

}
