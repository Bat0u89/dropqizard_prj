package com.osotnikov.revolut.homework.rest.response;

public class JsonResponseBody {
	
	int statusCode;
	String responseMessage;
	
	public JsonResponseBody() {
		super();
	}
	
	public JsonResponseBody(int statusCode, String errorMessage) {

		this.statusCode = statusCode;
		this.responseMessage = errorMessage;
	}

	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}
