package com.osotnikov.revolut.homework.dao.entity;

public interface IdentifiableEntity {
	
	Long getId();
}
