package com.osotnikov.revolut.homework.dao.service;

import java.math.BigDecimal;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osotnikov.revolut.homework.dao.entity.Account;
import com.osotnikov.revolut.homework.dao.entity.Transfer;
import com.osotnikov.revolut.homework.service.DatabaseService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Matchers.eq;

import javax.persistence.EntityExistsException;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseServiceTest {

	private final static Logger logger = LoggerFactory.getLogger(DatabaseServiceTest.class);
	
	@Mock
	SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@InjectMocks
	DatabaseService genericDao;
	
	private Account account = new Account();
	private Transfer transfer = new Transfer();
	
	@Before
	public void setup() {
		when(genericDao.currentSession()).thenReturn(session);
	}
	
	public DatabaseServiceTest() {
		
		account.setBalance(new BigDecimal(1000));
		account.setId(1L);
		account.setCurrencyCode("GBP");
		
		transfer.setBenefactorId(1L);
		transfer.setBeneficiaryId(2L);
		transfer.setId(1L);
		transfer.setTransferedAmount(new BigDecimal(500));
	}
	
	@Test
	public void fetchByIdSuccess() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(account);
		
		Account account2 = genericDao.fetchById(Account.class, 1L);
		logger.debug("Account was fetched: {}", ReflectionToStringBuilder.toString(account2));
		
		assertEquals(account, account2);
		
	}
	
	@Test
	public void fetchByIdFail() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(null);
		
		Account account2 = genericDao.fetchById(Account.class, 1L);
		logger.debug("Account was fetched: {}", account2);
		
		assertEquals(null, account2);
		
	}
	
	@Test
	public void fetchByIdWithExceptionSuccess() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(account);
		
		Account account2 = genericDao.fetchByIdWithException(Account.class, 1L);
		logger.debug("Account was fetched: {}", ReflectionToStringBuilder.toString(account2));
		
		assertEquals(account, account2);
		
	}
	
	@Test(expected = EntityNotFoundException.class)
	public void fetchByIdWithExceptionFail() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(null);
		
		genericDao.fetchByIdWithException(Account.class, 1L);
	}
	
	@Test
	public void entityExistsSuccess() {
		
		when(session.find(eq(Transfer.class), eq(1L))).thenReturn(transfer);
		
		boolean transferExists = genericDao.entityExists(Transfer.class, 1L);
		logger.debug("Transfer exists: {}", transferExists);
		
		assertEquals(true, transferExists);
		
	}
	
	@Test
	public void entityExistsFail() {
		
		when(session.find(eq(Transfer.class), eq(1L))).thenReturn(null);
		
		boolean transferExists = genericDao.entityExists(Transfer.class, 1L);
		logger.debug("Transfer exists: {}", transferExists);
		
		assertEquals(false, transferExists);
		
	}
	
	@Test
	public void createInStoreSuccess() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(null);
		
		genericDao.createInStore(Account.class, account);
		verify(session).persist(eq(account));
		
		logger.debug("Account was persisted: {}", ReflectionToStringBuilder.toString(account));
	}
	
	@Test(expected = EntityExistsException.class)
	public void createInStoreFail() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(account);
		
		genericDao.createInStore(Account.class, account);

	}
	
	@Test
	public void updateExistingSuccess() {
		
		when(session.find(eq(Account.class), eq(1L))).thenReturn(account);
		
		genericDao.updateExisting(Account.class, account);
		verify(session).merge(eq(account));
		
		logger.debug("Account was persisted: {}", ReflectionToStringBuilder.toString(account));
	}
	
	@Test(expected = EntityNotFoundException.class)
	public void updateExistingFail() {
		
		when(session.find(eq(Transfer.class), eq(1L))).thenReturn(null);
		
		genericDao.updateExisting(Transfer.class, transfer);
	}
	
	@Test(expected = NullPointerException.class)
	public void updateExistingError() {
		
		when(session.find(eq(Transfer.class), eq(1L))).thenReturn(null);
		
		genericDao.updateExisting(Transfer.class, null);
	}
	
}
